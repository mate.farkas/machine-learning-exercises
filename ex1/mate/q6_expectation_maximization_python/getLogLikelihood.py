import numpy as np

pi = np.pi

def getLogLikelihood(means, weights, covariances, X):
    # Log Likelihood estimation
    #
    # INPUT:
    # means          : Mean for each Gaussian KxD
    # weights        : Weight vector 1xK for K Gaussians
    # covariances    : Covariance matrices for each gaussian DxDxK
    # X              : Input data NxD
    # where N is number of data points
    # D is the dimension of the data points
    # K is number of gaussians
    #
    # OUTPUT:
    # logLikelihood  : log-likelihood
    
    #####Insert your code here for subtask 6a#####
    K = covariances.shape[-1]
    x = np.array(X)
    mu = np.array(means)
    N = x.shape[0]
    D = x.shape[1]
    w = np.array(weights).reshape((1,K))
    determinants = np.linalg.det(covariances.T)
    
    #calculating the inverse covariance matrices
    inverse_covariances = []
    for k in range(K):
        inverse_covariances.append(np.linalg.inv(covariances[:,:,k].T))
    inverse_covariances = np.array(inverse_covariances)
    
    logLikelihood = 0.0
    for n in range(N):
        logarg = 0.0
        for k in range(K):
            # exponential argument:
            xTSx = np.matmul((x[n,:]-mu[k,:]),np.matmul(inverse_covariances.T[:,:,k],x[n,:]-mu[k,:]))
            logarg+=w[:,k]*np.exp(-0.5*xTSx)/((2*pi)**(D/2)*determinants[k]**0.5)
        logLikelihood+=np.log(logarg)
    return logLikelihood

