import numpy as np
from getLogLikelihood import getLogLikelihood

pi = np.pi

def EStep(means, covariances, weights, X):
    # Expectation step of the EM Algorithm
    #
    # INPUT:
    # means          : Mean for each Gaussian KxD
    # weights        : Weight vector 1xK for K Gaussians
    # covariances    : Covariance matrices for each Gaussian DxDxK
    # X              : Input data NxD
    #
    # N is number of data points
    # D is the dimension of the data points
    # K is number of Gaussians
    #
    # OUTPUT:
    # logLikelihood  : Log-likelihood (a scalar).
    # gamma          : NxK matrix of responsibilities for N datapoints and K Gaussians.

    #####Insert your code here for subtask 6b#####
    K = covariances.shape[-1]
    x = np.array(X)
    mu = np.array(means)
    N = x.shape[0]
    D = x.shape[1]
    w = np.copy(weights).reshape((1,K))
    determinants = np.linalg.det(covariances.T)
    gamma = np.zeros((N,K))
    
    #calculating the inverse covariance matrices
    inverse_covariances = []
    for k in range(K):
        inverse_covariances.append(np.linalg.inv(covariances[:,:,k].T))
    inverse_covariances = np.array(inverse_covariances)
    
    for n in range(N):
        for k in range(K):
            xTSx = np.matmul((x[n,:]-mu[k,:]),np.matmul(inverse_covariances.T[:,:,k],x[n,:]-mu[k,:]))
            denominator = 0.0
            for k2 in range(K):
                xTSx2 = np.matmul((x[n,:]-mu[k2,:]),np.matmul(inverse_covariances.T[:,:,k2],x[n,:]-mu[k2,:]))
                denominator+=w[:,k2]*np.exp(-0.5*xTSx2)/((2*pi)**(D/2)*determinants[k]**0.5)
            gamma[n,k] = w[:,k]*np.exp(-0.5*xTSx)/((2*pi)**(D/2)*determinants[k]**0.5)/denominator
    logLikelihood = getLogLikelihood(means,weights,covariances,X)
    return [logLikelihood, gamma]
