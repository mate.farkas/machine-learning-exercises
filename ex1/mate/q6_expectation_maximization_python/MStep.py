import numpy as np
from getLogLikelihood import getLogLikelihood


def MStep(gamma, X):
    # Maximization step of the EM Algorithm
    #
    # INPUT:
    # gamma          : NxK matrix of responsibilities for N datapoints and K Gaussians.
    # X              : Input data (NxD matrix for N datapoints of dimension D).
    #
    # N is number of data points
    # D is the dimension of the data points
    # K is number of Gaussians
    #
    # OUTPUT:
    # logLikelihood  : Log-likelihood (a scalar).
    # means          : Mean for each gaussian (KxD).
    # weights        : Vector of weights of each gaussian (1xK).
    # covariances    : Covariance matrices for each component(DxDxK).

    #####Insert your code here for subtask 6c#####
    x = np.array(X)
    N = x.shape[0]
    D = x.shape[1]
    K = gamma.shape[1]
    Nj = np.sum(gamma,axis=0)
    weights = Nj/N
    means = np.zeros((K,D))
    for k in range(K):
        for n in range(N):
            means[k,:] += gamma[n,k] * x[n,:]/Nj[k]
    covariances = np.zeros((D,D,K))
    for k in range(K):
        for n in range(N):
            covariances[:,:,k] += gamma[n,k]*np.outer((x[n,:]-means[k,:]),(x[n,:]-means[k,:]))/Nj[k]
    logLikelihood = getLogLikelihood(means,weights,covariances,X)
    return weights, means, covariances, logLikelihood
