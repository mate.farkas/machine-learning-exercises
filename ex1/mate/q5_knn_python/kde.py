import numpy as np

pi = np.pi

def kde(samples, h):
    # compute density estimation from samples with KDE
    # Input
    #  samples    : DxN matrix of data points
    #  h          : (half) window size/radius of kernel
    # Output
    #  estDensity : estimated density in the range of [-5,5]

    #####Insert your code here for subtask 5a#####
    N = samples.shape[0]
    D = 1
    pos = np.arange(-5, 5.0, 0.1) # Returns a 100 dimensional vector
    estDensity = []
    for x in pos:
        estDensity.append(np.sum(np.exp(-np.power((samples-x),2)/(2*h**2))/(N*h*(2*pi)**(D/2))))
    # Compute the number of samples created
    estDensity = np.array(estDensity)
    res = np.array([pos,estDensity]).T
    return res