import numpy as np

def knn(samples, k):
    # compute density estimation from samples with KNN
    # Input
    #  samples    : DxN matrix of data points
    #  k          : number of neighbors
    # Output
    #  estDensity : estimated density in the range of [-5, 5]

    #####Insert your code here for subtask 5b#####
    N = samples.shape[0]
    pos = np.arange(-5, 5.0, 0.1) # Returns a 100 dimensional vector
    estDensity = []
    # With each iteration increment window size with epsilon,
    #  util k points are included
    epsilon = 0.001
    for x in pos:
        V = 0.0001
        K = 0
        while K<k:
            V += epsilon
            K = np.sum(np.ma.masked_where(np.abs(samples-x)<=V,samples).mask)
        estDensity.append(k/(N*2*V))
    estDensity = np.array(estDensity)
    res = np.array([pos,estDensity]).T
    # Compute the number of the samples created
    return res
