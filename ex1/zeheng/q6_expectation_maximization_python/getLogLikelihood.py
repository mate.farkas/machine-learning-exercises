import numpy as np
def getLogLikelihood(means, weights, covariances, X):
    # Log Likelihood estimation
    #
    # INPUT:
    # means          : Mean for each Gaussian KxD
    # weights        : Weight vector 1xK for K Gaussians
    # covariances    : Covariance matrices for each gaussian DxDxK
    # X              : Input data NxD
    # where N is number of data points
    # D is the dimension of the data points
    # K is number of gaussians
    #
    # OUTPUT:
    # logLikelihood  : log-likelihood

    #####Insert your code here for subtask 6a#####
    # N data points, D dimensions of the data points
    if len(X.shape) > 1:
        N, D = X.shape
    else:
        N = 1
        D = X.shape[0]
    
    # K Gaussians
    K = len(weights)
    
    logLikelihood = 0
    for i in range(N):
        prob = 0
        for j in range(K):
            if N == 1:
                diffMean = X - means[j]
            else:
                diffMean = X[i,:] - means[j]
            covariance = covariances[:,:,j].copy()
            norm = 1. / float(((2 * np.pi) ** (float(D) / 2.)) * np.sqrt(np.linalg.det(covariance)))
            prob += weights[j] * norm * np.exp(-0.5 * ((diffMean.T).dot(np.linalg.lstsq(covariance.T, diffMean.T)[0].T)))
        logLikelihood += np.log(prob)
    return logLikelihood

