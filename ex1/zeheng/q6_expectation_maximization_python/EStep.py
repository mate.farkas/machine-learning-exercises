import numpy as np
from getLogLikelihood import getLogLikelihood


def EStep(means, covariances, weights, X):
    # Expectation step of the EM Algorithm
    #
    # INPUT:
    # means          : Mean for each Gaussian KxD
    # weights        : Weight vector 1xK for K Gaussians
    # covariances    : Covariance matrices for each Gaussian DxDxK
    # X              : Input data NxD
    #
    # N is number of data points
    # D is the dimension of the data points
    # K is number of Gaussians
    #
    # OUTPUT:
    # logLikelihood  : Log-likelihood (a scalar).
    # gamma          : NxK matrix of responsibilities for N datapoints and K Gaussians.

    #####Insert your code here for subtask 6b#####
    logLikelihood = getLogLikelihood(means, weights, covariances, X)

    N, D = X.shape
    K = len(weights)

    gamma = np.zeros((N, K))
    for i in range(N):
        for j in range(K):
            diffMean = X[i] - means[j]
            covariance = covariances[:, :, j].copy()
            norm = 1./ float(((2 * np.pi) ** (float(D) / 2) * np.sqrt(np.linalg.det(covariances[:, :, j]))))
            gamma[i, j] = weights[j] * norm * np.exp(-0.5 * (diffMean.T.dot(np.linalg.lstsq(covariance.T, diffMean.T)[0].T)))
        gamma[i] /= gamma[i].sum()

    return [logLikelihood, gamma]
