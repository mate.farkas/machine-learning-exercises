import numpy as np
# might need to add path to mingw-w64/bin for cvxopt to work
# import os
# os.environ["PATH"] += os.pathsep + ...
import cvxopt


def svmlin(X, t, C):
    # Linear SVM Classifier
    #
    # INPUT:
    # X        : the dataset                  (num_samples x dim)
    # t        : labeling                     (num_samples x 1)
    # C        : penalty factor for slack variables (scalar)
    #
    # OUTPUT:
    # alpha    : output of quadprog function  (num_samples x 1)
    # sv       : support vectors (boolean)    (1 x num_samples)
    # w        : parameters of the classifier (1 x dim)
    # b        : bias of the classifier       (scalar)
    # result   : result of classification     (1 x num_samples)
    # slack    : points inside the margin (boolean)   (1 x num_samples)
    
    N,D = X.shape
    H = np.zeros((N,N))
    for n in range(N):
        for m in range(N):
            H[n,m] = t[n]*t[m]*X[n].dot(X[m])
    H = np.array(H)
    
    H = cvxopt.matrix(H,(N,N),'d')
    c = np.ones(N)*C
    T = cvxopt.matrix(t).T
    ones = np.ones((N,1))
    zeros = np.zeros(N)
    
    G = cvxopt.matrix(np.vstack([-np.eye(N),np.eye(N)]))
    h = cvxopt.matrix(np.hstack([zeros,c]))
    
    res = cvxopt.solvers.qp(H,cvxopt.matrix(-1*ones,(N,1),'d'),G=G,A=T,b=cvxopt.matrix(np.zeros(1)),h=h)
    
    alpha = np.array(res['x'])
    sv = np.ma.masked_where((1e-6<alpha) & (C>alpha), alpha).mask.reshape(-1)
    Nm = np.sum(sv)
    w = np.zeros(D)
    for n in range(N):
            w+=alpha[n]*t[n]*X[n]
    b = 0.0
    for n in range(Nm):
        b+=t[n]
        for m in range(Nm):
            b-=alpha[m]*t[m]*X[m].dot(X[n])
    b/=Nm
    
    dividerLine = w.dot(X.T)+b
    
    result = dividerLine.copy()
    result[np.where(result>0)] = 1
    result[np.where(result<0)] = -1

    slack = np.ma.masked_where(alpha>=1e-6, alpha).mask.reshape(-1)
    
    #####Insert your code here for subtask 2a#####
    return alpha, sv, w, b, result, slack
