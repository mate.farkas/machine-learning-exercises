import numpy as np


def leastSquares(data, label):
    # Sum of squared error shoud be minimized
    #
    # INPUT:
    # data        : Training inputs  (num_samples x dim)
    # label       : Training targets (num_samples x 1)
    #
    # OUTPUT:
    # weights     : weights   (dim x 1)
    # bias        : bias term (scalar)

    #####Insert your code here for subtask 1a#####
    # Extend each datapoint x as [1, x]
    # (Trick to avoid modeling the bias term explicitly)
    N, D = data.shape
    data_ext = np.append(np.ones((N,1)),data,axis=-1)
    weights = np.linalg.inv(data_ext.T @ data_ext) @ data_ext.T @ label
    weight = weights[1:]
    bias = weights[0]
    return weight, bias
